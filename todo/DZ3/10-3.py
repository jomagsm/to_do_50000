def degreee(num):
    answer3 = num % 3
    answer5 = num % 5
    if answer3 == 0 and answer5 == 0:
        return 'FizzBuzz'
    if answer3 == 0:
        return 'Fizz'
    if answer5 == 0:
        return 'Buzz'
    return num


num = int(input('Введите число: '))
print(degreee(num))